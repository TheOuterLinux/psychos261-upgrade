#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
HEIGHT=20
WIDTH=60
CHOICE_HEIGHT=20
BACKTITLE="Programs List --> Graphics --> Image Edit"
TITLE="Image Edit"
MENU="Choose one of the following options:"

OPTIONS=(1 "<-- Go back"
         2 "Batch editing (Still a work in progess)"
         3 "Blur"
         4 "Convert a file"
         5 "Compress image (jpeg)"
         6 "Contrast"
         7 "Despeckle"
         8 "Enhance - get rid of noise"
         9 "Flip image (vertical flip)"
         10 "Flop image (horizontal flop)"
         11 "Gamma"
         12 "Image info"
         13 "Monochromatic (black and white the image)"#
         14 "Resize a file")
         #15 "Reduce number of colors" #http://www.imagemagick.org/script/command-line-options.php#colors
         #16 "Rotate" #http://www.howtogeek.com/109369/how-to-quickly-resize-convert-modify-images-from-the-linux-terminal/ #http://www.dsl.org/cookbook/cookbook_23.html

CHOICE_IMAGEEDITMENU=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE_IMAGEEDITMENU in
        1)
            cd $DIR
            ./programs-list
            ;;
        2)
            cd $DIR
			./batch-image-edit
			exec 3>&-
            ;;
        3)
			cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck = "directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Blur Image" --title "Please choose an image to blur:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck = "directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 10 60
					fi
				done
				cp "$location" /tmp/
				templocation="/tmp/${location##*/}"
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo ""
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			imagecheck="1"
			while [ $imagecheck = "1" ]
			do
				cp "$location" /tmp/
				templocation="/tmp/${location##*/}"
				if radius=$(dialog --stdout --rangebox "What is the blur radius you'd like to set for $location? \nRadius refers to how how far to push each pixel.\nAnything higher than 5 is probably too much.\nUse Up/Down arrow keys to move slider." 10 60 0 8 0)
				then
					echo ""
				else
					cd $DIR
					./image-edit
				fi
				if strength=$(dialog --stdout --rangebox "What is the blur strength you'd like to set for $location? \nStrength refers to how strong the pixels blur from the start of each pixel to the end of the chosen radius before fading out.\nAnything higher than 8 is probably too much.\nUse Up/Down arrow keys to move slider." 10 60 0 8 0)
				then
					mogrify -blur "$radius"x"$strength" "$templocation"
				else
					cd $DIR
					./image-edit
				fi
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo ""
					echo "Showing edited image."
					echo "Quit the viewer to continue."
					viewnior "$templocation" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$templocation" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was the new image ok?" 6 75
				check="$?"
				case $check in
					0)
						cp "$templocation" "$location"
						rm -rf "$templocation"
						imagecheck="0"
						;;
					1)
						imagecheck="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			dialog --msgbox "Finished. Press ENTER to go back to the menu." 5 50
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        4)
			cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck = "directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Convert Image" --title "Please choose an image to convert:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck = "directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 10 60
					fi
				done
				cp "$location" /tmp/
				templocation="/tmp/${location##*/}"
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo ""
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to convert?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			if type=$(dialog --backtitle "$BACKTITLE --> Convert" --title "Please choose a format" --menu "" 12 40 20 \
				1 "JPEG" \
				2 "PNG" \
				3 "BMP" \
				4 "GIF" \
				5 "TIFF" \
				6 "PDF" 3>&1 1>&2 2>&3)
			then
				case $type in
					1)
						mogrify -format jpeg "$location"
						;;
					2)
						mogrify -format png "$location"
						;;
					3)
						mogrify -format bmp "$location"
						;;
					4)
						mogrify -format gif "$location"
						;;
					5)
						mogrify -format tiff "$location"
						;;
					6)
						mogrify -format pdf "$location"
						;;
				esac
			else
				cd $DIR
				./image-edit
			fi
			if [ "$type" = "1" ]; then
				type="JPEG"
			elif [ "$type" = "2" ]; then
				type="PNG"
			elif [ "$type" = "3" ]; then
				type="BMP"
			elif [ "$type" = "4" ]; then
				type="BMP"
			elif [ "$type" = "5" ]; then
				type="TIFF"
			elif [ "$type" = "6" ]; then
				type="PDF"
			fi
			dialog --yesno "File $loaction has been converted to $type.\n Keep original file?" 6 40
				check="$?"
				case $check in
					0)
						dialog --msgbox "The file $location was KEPT." 6 75
						;;
					1)
						rm -rf "$location"
						dialog --msgbox "The file $location was DELETED." 6 75
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
            ./image-edit
            exec 3>&-
            ;;
        5)
			cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Compress Image" --title "Please choose an image to compress:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 10 60
					fi
				done
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			echo ""
            filename=${location%.*}
            directory="${location%/*}"
            cd "$directory"
            if compression=$(dialog --stdout --rangebox "What is the quality level (JPEG) you'd like to set for $location? \nUse Up/Down arrow keys to move slider." 10 60 0 100 80)
			then
				convert "$location" -quality $compression "$filename.jpg"
			else
				cd $DIR
				./image-edit
			fi
			dialog --yesno "File $location has been converted to JPEG. \nKeep original file?" 8 75
				check="$?"
				case $check in
					0)
						dialog --msgbox "Original file $location was KEPT." 6 75
						;;
					1)
						rm -rf "$location"
						#rm -rf "$filename.png" "$filename.bmp" "$filename.gif" "$filename.tiff" "$filename.tif" "$filename.jpeg"
						dialog --msgbox "Original file $location was DELETED." 6 75
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        6)
            cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> +/- Contrast Image" --title "Please choose an image to change contrast:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 10 60
					fi
				done				
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			imagecheck="1"
			while [ $imagecheck = "1" ]
			do
				cp "$location" /tmp/
				templocation="/tmp/${location##*/}"
				if amount=$(dialog --stdout --rangebox "What is the contrast you'd like to set for $location?\nUse Up/Down arrow keys to move slider.\nBe careful which digit the curser is on when moving the slider." 10 60 -20 20 0)
				then
					if [ $amount -ge 1 ]
					then
						i=1
						while [ $i -le $amount ]
						do
							#Increase contrast
							for i in $(seq 0 10 100) ; do mogrify -contrast "$templocation"; sleep 1; echo $i | dialog --title "Increasing contrast..." --gauge "Please wait" 6 60 0; done
							i=$(( $i+1 ))
						done
					elif [ $amount -le -1 ]
					then
						i=1
						absolutevalueamount=$(echo "$amount" | sed 's/-//g') #absolute value convert (remove negative symbol)
						while [ $i -le "$absolutevalueamount" ]
						do
							#Decrease contrast
							for i in $(seq 0 10 100) ; do mogrify +contrast "$templocation"; sleep 1; echo $i | dialog --title "Decreasing contrast..." --gauge "Please wait" 6 60 0; done
							i=$(( $i+1 ))
						done
					else
						dialog --msgbox "Image contrast was not changed." 6 40
					fi
				else
					cd $DIR
					./image-edit
				fi
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo ""
					echo "Showing edited image."
					echo "Quit the viewer to continue."
					viewnior "$templocation" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$templocation" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was the new image ok?" 5 27
				check="$?"
				case $check in
					0)
						cp "$templocation" "$location"
						rm -rf "$templocation"
						imagecheck="0"
						;;
					1)
						imagecheck="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			dialog --msgbox "Finished. Press ENTER to go back to the menu." 5 50
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        7)
			cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Despeckle Image" --title "Please choose an image to despeckle:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 6 60
					fi
				done				
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			imagecheck="1"
			while [ $imagecheck = "1" ]
			do
				cp "$location" /tmp/
				templocation="/tmp/${location##*/}"
				if amount=$(dialog --stdout --rangebox "What is the despeckle amount you'd like to set for $location?\nUse Up/Down arrow keys to move slider.\nBe careful which digit the curser is on when moving the slider." 10 60 0 20 0)
				then
					if [ $amount -ge 1 ]
					then
						i=1
						while [ $i -le $amount ]
						do
							for i in $(seq 0 10 100) ; do mogrify -despeckle "$templocation"; sleep 1; echo $i | dialog --title "Despeckling image.." --gauge "Please wait" 6 60 0; done
							i=$(( $i+1 ))
						done
					else
						dialog --msgbox "Image despeckle was not changed." 6 40
					fi
				else
					cd $DIR
					./image-edit
				fi
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo ""
					echo "Showing edited image."
					echo "Quit the viewer to continue."
					viewnior "$templocation" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$templocation" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was the new image ok?" 5 27
				check="$?"
				case $check in
					0)
						cp "$templocation" "$location"
						rm -rf "$templocation"
						imagecheck="0"
						;;
					1)
						imagecheck="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			dialog --msgbox "Finished. Press ENTER to go back to the menu." 5 50
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        8)
            cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Enhance Image" --title "Please choose an image to enhance:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 6 60
					fi
				done				
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			imagecheck="1"
			while [ $imagecheck = "1" ]
			do
				cp "$location" /tmp/
				templocation="/tmp/${location##*/}"
				if amount=$(dialog --stdout --rangebox "What is the enhance amount you'd like to set for $location?\nUse Up/Down arrow keys to move slider.\nBe careful which digit the curser is on when moving the slider." 10 60 0 20 0)
				then
					if [ $amount -ge 1 ]
					then
						i=1
						while [ $i -le $amount ]
						do
							for i in $(seq 0 10 100) ; do mogrify -enhance "$templocation"; sleep 1; echo $i | dialog --title "Enhancing image.." --gauge "Please wait" 6 60 0; done
							i=$(( $i+1 ))
						done
					else
						dialog --msgbox "Image was not enhanced." 6 40
					fi
				else
					cd $DIR
					./image-edit
				fi
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo ""
					echo "Showing edited image."
					echo "Quit the viewer to continue."
					viewnior "$templocation" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$templocation" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was the new image ok?" 5 27
				check="$?"
				case $check in
					0)
						cp "$templocation" "$location"
						rm -rf "$templocation"
						imagecheck="0"
						;;
					1)
						imagecheck="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			dialog --msgbox "Finished. Press ENTER to go back to the menu." 5 50
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        9)
            cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Flip Image" --title "Please choose an image to (vertical) flip:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 6 60
					fi
				done		
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			echo ""
			mogrify -flip "$location"
			TTY=$(tty)
			TMP=${TTY%/*}
			TMP=${TMP##*/}
			if [ "$TMP" == "pts" ]
			then
				echo ""
				echo "Showing edited image."
				echo "Quit the viewer to continue."
				viewnior "$location" #Will only work in GUI mode
			else
				fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
			fi
			dialog --msgbox "Finished. Press ENTER to go back to the menu." 5 50
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        10)
            cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Flop Image" --title "Please choose an image to (horizontal) flop:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 6 60
					fi
				done		
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			echo ""
			mogrify -flop "$location"
			TTY=$(tty)
			TMP=${TTY%/*}
			TMP=${TMP##*/}
			if [ "$TMP" == "pts" ]
			then
				echo ""
				echo "Showing edited image."
				echo "Quit the viewer to continue."
				viewnior "$location" #Will only work in GUI mode
			else
				fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
			fi
			dialog --msgbox "Finished. Press ENTER to go back to the menu." 5 50
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        11)
            cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Gamma" --title "Please choose an image to change gamma:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 6 60
					fi
				done				
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			imagecheck="1"
			while [ $imagecheck = "1" ]
			do
				cp "$location" /tmp/
				templocation="/tmp/${location##*/}"
				if amount=$(dialog --stdout --rangebox "What is the gamma (30 = 3.0) you'd like to set for $location?\n \nUse Up/Down arrow keys to move slider.\nBe careful which digit the curser is on when moving the slider." 10 60 0 30 0)
				then
					decimalamount=$(echo "scale=1; $amount / 10" | bc )
					if [ $amount -ge 1 ]
					then
						mogrify -gamma "$decimalamount" "$templocation"
					else
						dialog --msgbox "Image gamma was not changed." 6 40
					fi
				else
					cd $DIR
					./image-edit
				fi
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo ""
					echo "Showing edited image."
					echo "Quit the viewer to continue."
					viewnior "$templocation" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$templocation" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was the new image ok?" 5 27
				check="$?"
				case $check in
					0)
						cp "$templocation" "$location"
						rm -rf "$templocation"
						imagecheck="0"
						;;
					1)
						imagecheck="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			dialog --msgbox "Finished. Press ENTER to go back to the menu." 5 50
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        12)
            cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Image Info" --title "Please choose an image to change gamma:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 6 60
					fi
				done				
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "View information of image $location?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			info=$(mogrify -identify -verbose "$location")
			echo "$info" > /tmp/imageinfolog.txt
			dialog --title "Graphics -> Image Edit -> Image Information" --textbox /tmp/imageinfolog.txt 32 80
			rm -rf /tmp/imageinfolog.txt
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
			
        13)
            cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Monochrome Image" --title "Please choose an image to black & white:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 6 60
					fi
				done				
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			imagecheck="1"
			while [ $imagecheck = "1" ]
			do
				cp "$location" /tmp/
				templocation="/tmp/${location##*/}"
				mogrify -monochrome "$templocation"
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo ""
					echo "Showing edited image."
					echo "Quit the viewer to continue."
					viewnior "$templocation" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$templocation" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was the new image ok?" 5 27
				check="$?"
				case $check in
					0)
						cp "$templocation" "$location"
						rm -rf "$templocation"
						imagecheck="0"
						;;
					1)
						foldercheck="directory"
						while [ $foldercheck="directory" ]
						do
							if location=$(dialog --stdout --backtitle "$BACKTITLE --> Monochrome Image" --title "Please choose an image to black & white:" --fselect $HOME/Pictures/ 10 75)
							then
								foldercheck=$(file -b "$location" | grep "")
							else
								cd $DIR
								./image-edit
							fi
							if [ $foldercheck="directory" ]
							then
								dialog --msgbox "Please choose a file and not a folder." 6 60
							fi
						done	
						imagecheck="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			dialog --msgbox "Finished. Press ENTER to go back to the menu." 5 50
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        14)
            cd $DIR
			clear
			check="1"
			while [ $check = "1" ]
			do
				foldercheck="directory"
				while [ $foldercheck="directory" ]
				do
					if location=$(dialog --stdout --backtitle "$BACKTITLE --> Resize Image" --title "Please choose an image to resize:" --fselect $HOME/Pictures/ 10 75)
					then
						foldercheck=$(file -b "$location" | grep "")
					else
						cd $DIR
						./image-edit
					fi
					if [ $foldercheck="directory" ]
					then
						dialog --msgbox "Please choose a file and not a folder." 6 60
					fi
				done				
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo "Showing selected image."
					echo "Quit the viewer to continue."
					viewnior "$location" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$location" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was $location the correct image to edit?" 6 75
				check="$?"
				case $check in
					0)
						check="0"
						;;
					1)
						check="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			imagecheck="1"
			while [ $imagecheck = "1" ]
			do
				cp "$location" /tmp/
				templocation="/tmp/${location##*/}"
				numbercheck="1"
				while [ "$numbercheck" = "1" ]
				do
					if width=$(dialog --inputbox "Enter new width for image (pixels):" 10 50 3>&1 1>&2 2>&3)
					then
						if echo "$width" | egrep -q '^[0-9]*\.?[0-9]+$'
						then
							numbercheck="0"
						else
							dialog --msgbox "$width is an invalid entry. Only positive numbers accepted." 10 75
						fi
					else
						cd $DIR
						./image-edit
					fi
				done
				numbercheck="1"
				while [ "$numbercheck" = "1" ]
				do
					if height=$(dialog --inputbox "Enter new height for image (pixels):" 10 50 3>&1 1>&2 2>&3)
					then
						if echo "$height" | egrep -q '^[0-9]*\.?[0-9]+$'
						then
							numbercheck="0"
						else
							dialog --msgbox "$height is an invalid entry. Only positive numbers accepted." 10 75
							numbercheck="1"
						fi
					else
						cd $DIR
						./image-edit
					fi
				done
				dialog --yesno "Preserve ratio for $location?" 6 75
				ratio="$?"
				case $ratio in
					0)
						mogrify -resize "$width"x"$height" "$templocation"
						;;
					1)
						mogrify -resize "$width"x"$height"! "$templocation" #Backwards?
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
				TTY=$(tty)
				TMP=${TTY%/*}
				TMP=${TMP##*/}
				if [ "$TMP" == "pts" ]
				then
					echo ""
					echo "Showing edited image."
					echo "Quit the viewer to continue."
					viewnior "$templocation" #Will only work in GUI mode
				else
					fbi -noverbose -a -u "$templocation" #Will only work in command line mode #Will only work in command line mode
				fi
				dialog --yesno "Was the new image ok?" 5 27
				check="$?"
				case $check in
					0)
						cp "$templocation" "$location"
						rm -rf "$templocation"
						imagecheck="0"
						;;
					1)
						imagecheck="1"
						;;
					255)
						cd $DIR
						./image-edit
						;;
				esac
			done
			dialog --msgbox "Finished. Press ENTER to go back to the menu." 5 50
            cd $DIR
            ./image-edit
            exec 3>&-
            ;;
        255)
            cd $DIR
            ./programs-list
            ;;
esac
