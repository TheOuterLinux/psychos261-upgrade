uparrow=$'\x1b[A'
downarrow=$'\x1b[B'
leftarrow=$'\x1b[D'
rightarrow=$'\x1b[C'
spacebar=$'aaa'
escape=$'\x1b'


key=-1
ship1="^" #ship line 1
ship2="[|]" #ship line 2
gobbler1="M" #gobbler line 1
gobbler2="<=>" #gobbler line 2
gobblerd1="*" #gobblerd line 1
gobblerd2="***" #gobblerd line 2
ship=(14 19)
shipold=(13 19)
gameheight=20
gamewidth=25
level=1
remaining=0
levelloaded=0
timer=0
life=100
bank=0

bullets=(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
bulletsold=(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
gobblers=(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
gobblersold=(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
gobblersbul=(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
gobblersbulold=(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
money=(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
moneyold=(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)


initialize(){
  clear
  tput civis  #hide cursor
  stty -echo -icanon time 0 min 0
  printgameboarders
}

cleanup(){
  tput cnorm #show cursor
  stty sane
  clear
  exit
}

printgameboarders (){
	for ((i=0; i<= $(($gameheight+2)); i++));	do
    tput cup $i 0
		echo "##"
		tput cup $i $gamewidth
		if [[ $i -gt 1 && $i -lt $(($gameheight-2)) ]]; then
		  echo "###   ##########"
		else
		  echo "################"
		fi
	done
	for ((i=0; i<$gamewidth+2; i++)); do
    tput cup 0 $i
		echo "#"
		tput cup $(($gameheight+2)) $i
		echo "#"
	done    
	tput cup 2 $(($gamewidth+4))
	echo "L"
  tput cup 2 $(($gamewidth+8))
  echo " LVL "
  tput cup 3 $(($gamewidth+8))
  echo "     "
  tput cup 6 $(($gamewidth+8))
  echo " REM "
  tput cup 7 $(($gamewidth+8))
  echo "     "
  tput cup 10 $(($gamewidth+8))
  echo " BNK "
  tput cup 11 $(($gamewidth+8))
  echo "     "
  
	updatelife
	updatebank
}

updatelife(){
    for ((i=1; i<= $(($gameheight-5)); i++));	do
      tput cup $(($gameheight-2-$i)) $(($gamewidth+4))
      if [ $i -lt "$((($life * $(($gameheight-4))) / 100))" ]; then
  	  	echo "."
  	  else
  	    tput ech 1
  	  fi
	  done
}

updatelevel(){
  tput cup 3 $(($gamewidth+8))
  echo "  $level  "
}

updatebank(){
  tput cup 11 $(($gamewidth+8))
  echo "  $bank  "
}

updateship(){
	tput cup ${shipold[1]} ${shipold[0]}
	tput ech 1
	tput cup $((${shipold[1]}+1)) $((${shipold[0]}-1)) 
  tput ech 3

	tput cup ${ship[1]} ${ship[0]}
	echo "$ship1"
	tput cup $((${ship[1]}+1)) $((${ship[0]}-1)) 
	echo "$ship2"

  let 'shipold[0] = ship[0]'
  let 'shipold[1] = ship[1]'
}

updategobblers(){
	for ((i=0; i<${#gobblers[@]}; i+=2)); do
	  for ((j=0; j<${#gobblers[@]}; j+=2)); do
  	  if [[ ${gobblers[i]} -ne ${gobblersold[i]} || ${gobblers[i+1]} -ne ${gobblersold[i+1]} ]]; then 	  
    	  tput cup $((${gobblersold[i+1]}+1)) ${gobblersold[i]}
    	  tput ech 1
	    	tput cup ${gobblersold[i+1]} $((${gobblersold[i]}-1))
        tput ech 3
       
        let 'gobblersold[i] = gobblers[i]'
        let 'gobblersold[i+1] = gobblers[i+1]'
        
        #print new gobblers
    	  if [ "${gobblers[i]}" -gt 0 ]; then
    		  tput cup $((${gobblers[i+1]}+1)) ${gobblers[i]}
          echo "$gobbler1"
    		 	tput cup ${gobblers[i+1]} $((${gobblers[i]}-1))
    	    echo "$gobbler2"
    	  fi
  	  fi  
    done
  done
}

updateshipbullets(){
	for ((i=0; i<${#bullets[@]}; i+=2)); do
	  for ((j=0; j<${#bullets[@]}; j+=2)); do
  	  if [[ ${bullets[i]} -ne ${bulletsold[i]} || ${bullets[i+1]} -ne ${bulletsold[i+1]} ]]; then
        if [[ $((${bulletsold[i+1]})) -ne 0 && ${bulletsold[i]} -ne 0 ]]; then 
  	      tput cup $((${bulletsold[i+1]})) ${bulletsold[i]}
	  	    tput ech 1
	  	  fi  
        let 'bulletsold[i] = bullets[i]'
        let 'bulletsold[i+1] = bullets[i+1]'

        #print bullet
        if [ "${bullets[i+1]}" -gt 0 ]; then
    	    tput cup $((${bullets[i+1]})) ${bullets[i]}
          echo "|"
        fi
      fi
    done
  done
}

updategobblerbullets(){
	for ((i=0; i<${#gobblersbul[@]}; i+=2)); do
	  for ((j=0; j<${#gobblersbul[@]}; j+=2)); do
  	  if [[ ${gobblersbul[i]} -ne ${gobblersbulold[i]} || ${gobblersbul[i+1]} -ne ${gobblersbulold[i+1]} ]]; then
        if [[ $((${gobblersbulold[i+1]})) -ne 0 && ${gobblersbulold[i]} -ne 0 ]]; then
    	    tput cup $((${gobblersbulold[i+1]})) ${gobblersbulold[i]}
	    	  tput ech 1
	  	  fi
        let 'gobblersbulold[i] = gobblersbul[i]'
        let 'gobblersbulold[i+1] = gobblersbul[i+1]'

        #print bullet
        if [ "${gobblersbul[i+1]}" -gt 0 ]; then
    	    tput cup $((${gobblersbul[i+1]})) ${gobblersbul[i]}
          echo "|"
        fi
      fi
    done
  done
}

updatemoney(){
	for ((i=0; i<${#money[@]}; i+=2)); do
	  for ((j=0; j<${#money[@]}; j+=2)); do
  	  if [[ ${money[i]} -ne ${moneyold[i]} || ${money[i+1]} -ne ${moneyold[i+1]} ]]; then
        if [[ $((${moneyold[i+1]})) -ne 0 && ${moneyold[i]} -ne 0 ]]; then
    	    tput cup $((${moneyold[i+1]})) ${moneyold[i]}
	    	  tput ech 1
	  	  fi
        let 'moneyold[i] = money[i]'
        let 'moneyold[i+1] = money[i+1]'

        #print bullet
        if [ "${money[i+1]}" -gt 0 ]; then
    	    tput cup $((${money[i+1]})) ${money[i]}
          echo "$"
        fi
      fi
    done
  done
}

countremaining(){
    remainingtemp=0
		for ((i=0; i<${#gobblers[@]}; i+=2)); do
  		if [ "${gobblers[i]}" -gt 0 ]; then
  		  let 'remainingtemp++'
  	  fi
  	done

    if [ $remainingtemp -ne $remaining ]; then
        tput cup 7 $(($gamewidth+8))
        let 'remaining=remainingtemp'
        echo "  $remaining  " 
    fi       
}

resetbullets(){
  for ((i=0; i<${#gobblersbul[@]}; i++)); do
    let 'gobblersbul[i]=0'
  done
  for ((i=0; i<${#bullets[@]}; i++)); do
    let 'bullets[i]=0'
  done
  updategobblerbullets
  updateshipbullets
}

printleveltitle(){
    updatelevel
    tput cup 4 5
    echo "#################"
    tput cup 5 5
    if [ $1 -lt 10 ]; then
      echo "#### LEVEL $1 ####"
    else
      echo "#### LEVEL $1 ###"
    fi
    tput cup 6 5
    echo "#################"
    sleep 1
    tput cup 5 5
    echo "####   GO!   ####"
    sleep 0.8
    tput cup 4 5
    tput ech 18
    tput cup 5 5
    tput ech 18
    tput cup 6 5
    tput ech 18
}

levelone(){
  if [ "$levelloaded" -eq 0 ];then
    
    printleveltitle 1
    let 'life=100'
    updatelife
    
    #spawn gobblers
    for ((i=0; i<10; i++)); do
      let 'gobblersold[i]=7'
    done
    let 'gobblers[0]=5'
    let 'gobblers[1]=5'
    let 'gobblers[2]=12'
    let 'gobblers[3]=5'
    let 'gobblers[4]=20'
    let 'gobblers[5]=5'
    let 'gobblers[6]=8'
    let 'gobblers[7]=10'
    let 'gobblers[8]=16'
    let 'gobblers[9]=10'

    updategobblers
    updateship
    sleep .5
    
		let 'levelloaded=1'
  else
	  if [ "$(($timer%30))" -eq 0 ]; then
  	  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then
		  		let 'gobblers[i+1]-=1'
		  	fi
		  done
		  updategobblers
		elif [ "$(($timer%15))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i+1]+=1'
			  fi
			done
			updategobblers
		fi
	fi 
}

leveltwo(){
  if [ "$levelloaded" -eq 0 ];then
    
    printleveltitle 2
       
    #spawn gobblers
    for ((i=0; i<10; i++)); do
      let 'gobblersold[i]=7'
    done
    let 'gobblers[0]=5'
    let 'gobblers[1]=5'
    let 'gobblers[2]=12'
    let 'gobblers[3]=5'
    let 'gobblers[4]=20'
    let 'gobblers[5]=5'
    let 'gobblers[6]=8'
    let 'gobblers[7]=10'
    let 'gobblers[8]=16'
    let 'gobblers[9]=10'

    updategobblers
    updateship
    sleep .5
    
		let 'levelloaded=1'
  else
	  if [ "$(($timer%80))" -eq 0 ]; then
  	  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then
		  		let 'gobblers[i]-=1'
		  	fi
		  done
		  updategobblers
		elif [ "$(($timer%60))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i]-=1'
			  fi
			done
			updategobblers
		elif [ "$(($timer%40))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i]+=1'
			  fi
			done
			updategobblers
		elif [ "$(($timer%20))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i]+=1'
			  fi
			done
			updategobblers
		fi
	fi 
}

levelthree(){
  if [ "$levelloaded" -eq 0 ];then
    
    printleveltitle 3
              
    #spawn gobblers
    for ((i=0; i<16; i++)); do
      let 'gobblersold[i]=7'
    done
    let 'gobblers[0]=5'
    let 'gobblers[1]=5'
    let 'gobblers[2]=13'
    let 'gobblers[3]=5'
    let 'gobblers[4]=21'
    let 'gobblers[5]=5'
    let 'gobblers[6]=8'
    let 'gobblers[7]=10'
    let 'gobblers[8]=16'
    let 'gobblers[9]=10'
    let 'gobblers[10]=0'
    let 'gobblers[11]=0'
    let 'gobblers[12]=11'
    let 'gobblers[13]=2'
    let 'gobblers[14]=19'
    let 'gobblers[15]=2'
                
    updategobblers
    updateship
    sleep .5
    
		let 'levelloaded=1'
  else
	  if [ "$(($timer%80))" -eq 0 ]; then
  	  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then
		  		let 'gobblers[i]-=1'
		  	fi
		  done
		  updategobblers
		elif [ "$(($timer%60))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i]-=1'
			  fi
			done
			updategobblers
		elif [ "$(($timer%40))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i]+=1'
			  fi
			done
			updategobblers
		elif [ "$(($timer%20))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i]+=1'
			  fi
			done
			updategobblers
		fi
	fi 
}

levelfour(){
  if [ "$levelloaded" -eq 0 ];then
    goshopping
    printleveltitle 4
              
    #spawn gobblers
    for ((i=0; i<14; i++)); do
      let 'gobblersold[i]=3'
    done
    let 'gobblers[0]=5'
    let 'gobblers[1]=2'
    let 'gobblers[2]=7'
    let 'gobblers[3]=4'
    let 'gobblers[4]=9'
    let 'gobblers[5]=6'
    let 'gobblers[6]=11'
    let 'gobblers[7]=8'
    let 'gobblers[8]=13'
    let 'gobblers[9]=6'
    let 'gobblers[10]=15'
    let 'gobblers[11]=4'
    let 'gobblers[12]=17'
    let 'gobblers[13]=2'

    updategobblers
    updateship
    sleep .5
    
		let 'levelloaded=1'
  else
	  if [ "$(($timer%80))" -eq 0 ]; then
  	  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then
		  		let 'gobblers[i]-=1'
		  	fi
		  done
		  updategobblers
		elif [ "$(($timer%60))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i]-=1'
			  fi
			done
			updategobblers
		elif [ "$(($timer%40))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i]+=1'
			  fi
			done
			updategobblers
		elif [ "$(($timer%20))" -eq 0 ]; then
		  for ((i=0; i<${#gobblers[@]}; i+=2)); do
        if [ "${gobblers[i]}" -ne 0 ]; then          
					let 'gobblers[i]+=1'
			  fi
			done
			updategobblers
		fi
	fi 
}

shiphit(){
  let 'life-=20'
  updatelife
  if [ $life -lt 0 ]; then
    endgame
  fi
}

cleargame(){
  for ((i=0; i<${#bullets[@]}; i++)); do
    let 'bullets[i]=0'
  done
  for ((i=0; i<${#gobblers[@]}; i++)); do
    let 'gobblers[i]=0'
  done
  for ((i=0; i<${#gobblersbul[@]}; i++)); do
    let 'gobblersbul[i]=0'
  done
  updateshipbullets
  updategobblers
  updategobblerbullets
}

endgame(){
  cleargame
  tput cup 4 5
  echo "#################"
  tput cup 5 5
  echo "### GAME OVER ###"
  tput cup 6 5
  echo "#################"
  sleep 1
  tput cup 8 7
  echo "1) Play Again"
  tput cup 9 7
  echo "2) Quit"

  while [ == ]
    do
	    key=$(dd bs=3 count=1 2>/dev/null)
    	case "$key" in	
	    '1')
          let 'level=1'
          let 'levelloaded=0'
        break
		   ;;
	     $escape|q|2)
         cleanup
       ;;	
    esac
  done
 
  tput cup 4 5
  tput ech 18
  tput cup 5 5
  tput ech 18
  tput cup 6 5
  tput ech 18
  tput cup 8 7
  tput ech 13
  tput cup 9 7
  tput ech 10
}
goshopping(){
  cleargame
  tput cup 4 5
  echo "##################"
  tput cup 5 5
  echo "#### UPGRADES ####"
  tput cup 6 5
  echo "##################"
  sleep 1
  tput cup 8 5
  echo "1) HEALTH    ~150"
  tput cup 9 5
  echo "2) WEAPON 2  ~750"
  tput cup 10 5
  echo "3) WEAPON 1  ~600"
  tput cup 11 5
  echo "4) QUIT"


  while [ == ]
    do
	    key=$(dd bs=3 count=1 2>/dev/null)
    	case "$key" in	
	    '1')
	        if [ $bank -ge 150 ]; then
            let 'life+=10'
            let 'bank-=150'
            updatebank
            updatelife
            tput cup 15 6
            echo "### LIFE += 10 ###"
          else
            tput cup 15 6
            echo "###INSUFFICIENT###"
          fi
		   ;;
	     $escape|q|4)
         break
       ;;	
    esac
  done
 
  tput cup 4 5
  tput ech 18
  tput cup 5 5
  tput ech 18
  tput cup 6 5
  tput ech 18
  tput cup 8 5
  tput ech 17
  tput cup 9 5
  tput ech 17
  tput cup 10 5
  tput ech 17
  tput cup 11 5
  tput ech 13
  tput cup 15 5
  tput ech 19

}

win(){
  cleargame
  tput cup 4 5
  echo "##################"
  tput cup 5 5
  echo "#### YOU WIN! ####"
  tput cup 6 5
  echo "##################"
  sleep 1
  tput cup 8 7
  echo "1) Play Again"
  tput cup 9 7
  echo "2) Quit"

  while [ == ]
    do
	    key=$(dd bs=3 count=1 2>/dev/null)
    	case "$key" in	
	    '1')
          let 'level=1'
          let 'levelloaded=0'
        break
		   ;;
	     $escape|q|2)
         cleanup
       ;;	
    esac
  done
 
  tput cup 4 5
  tput ech 18
  tput cup 5 5
  tput ech 18
  tput cup 6 5
  tput ech 18
  tput cup 8 7
  tput ech 13
  tput cup 9 7
  tput ech 10
}

initialize

#start game loop
while [ == ]
do
	key=$(dd bs=3 count=1 2>/dev/null)
	case "$key" in
    $uparrow|w)
      if [ "${ship[1]}" -gt  $(($gameheight-5)) ]; then
			  let "ship[1]--"
			  updateship
			fi
	  ;;
    $downarrow|s)
     if [ "${ship[1]}" -lt $gameheight ]; then
		   let "ship[1]++"
			 updateship
	   fi
		 ;;
	  $leftarrow|a)
     if [ "${ship[0]}" -gt  3 ]; then
		   let "ship[0]--"
			 updateship
		 fi
     ;;
     $rightarrow|d)
			 if [ "${ship[0]}" -lt $(($gamewidth-2)) ]; then
		   let "ship[0]++"
			 updateship
		 fi
     ;;
	   ' ')#spacebar shoots
		 	for ((i=0; i<${#bullets[@]}; i+=2))
      do
	      if [ "${bullets[i]}" -eq 0 ]; then
	        let "bullets[i] = ship[0]" 
          let "bullets[i+1] = ship[1]-1"
					let "i = ${#bullets[@]}"
	      fi
      done			 
		 ;;
	   $escape|q)
       cleanup
     ;;	
  esac
                      
  updateshipbullets
  updategobblerbullets
  updatemoney

  #Gobblers Randomly Shoot
  for ((i=0; i<${#gobblers[@]}; i+=2)); do
		#randomly make a bullet for exising gobblers
		if [ "${gobblers[i]}" -gt 0 ]; then
		  if [ $((RANDOM%30)) -eq 0 ]; then
        for ((j=0; j<${#gobblersbul[@]}; j+=2))
        do
	        if [ "${gobblersbul[j]}" -eq 0 ]; then
	          let "gobblersbul[j] = gobblers[i]" 
            let "gobblersbul[j+1] = gobblers[i+1]+1"
					  let "j = ${#gobblersbul[@]}"
	        fi
        done
		  fi
		fi
	done

  #check for collision between bullet and gobblers
	for ((i=0; i<${#gobblers[@]}; i+=2)); do	
		for ((j=0; j<${#bullets[@]}; j+=2));do
			if [ "${gobblers[i]}" -gt 0 ]; then
			  if [[ $((${bullets[j]} -${gobblers[i]})) -gt -2 && $((${bullets[j]} -${gobblers[i]})) -lt 2 ]]; then
	        if [ $(($((${gobblers[i+1]})) -$((${bullets[j+1]})))) -eq 0 ]; then
    				let 'bullets[j]=0'
					  let 'bullets[j+1]=0'
            tput cup $((${gobblers[i+1]}+1)) ${gobblers[i]}
            echo "$gobblerd1"
			      tput cup ${gobblers[i+1]} $((${gobblers[i]}-1))
	          echo "$gobblerd2"
		        sleep 0.05
		        if [ $((RANDOM%3)) -eq 0 ]; then
              for ((k=0; k<${#money[@]}; k+=2))
              do
	              if [ "${money[k]}" -eq 0 ]; then
	                let "money[k] = gobblers[i]" 
                  let "money[k+1] = gobblers[i+1]+1"
					        let "j = ${#money[@]}"
	              fi
              done
		        fi
			      let 'gobblers[i]=0'
            let 'gobblers[i+1]=0' 
            updategobblers
				  fi
	    	fi	
	    fi
		done
  done


  #check for collision between bullet and player
	for ((i=0; i<${#gobblersbul[@]}; i+=2));do
		if [ "${gobblersbul[i]}" -ne 0 ]; then
		  if [[ $((${gobblersbul[i]} -${ship[0]})) -gt -2 && $((${gobblersbul[i]} -${ship[0]})) -lt 2 ]]; then
        if [ $(($((${ship[1]})) -$((${gobblersbul[i+1]})))) -eq 0 ]; then
          shiphit
			  fi
    	fi	
    fi
	done

  #check for collision between money and player
	for ((i=0; i<${#money[@]}; i+=2));do
		if [ "${money[i]}" -ne 0 ]; then
		  if [[ $((${money[i]} -${ship[0]})) -gt -2 && $((${money[i]} -${ship[0]})) -lt 2 ]]; then
        if [[ $(($((${ship[1]})) -$((${money[i+1]})))) -lt 2 &&  $(($((${ship[1]})) -$((${money[i+1]})))) -gt -1 ]]; then
          let 'bank+=10'
          updatebank
          let 'money[i]=0'
          let 'money[i+1]=0'
			  fi
    	fi	
    fi
	done

  #incriment the player bullets
	for ((i=1; i<${#bullets[@]}; i+=2))
  do
	  if [ "${bullets[i-1]}" -ne 0 ]; then
		  let "bullets[i]--"		
		  if [ "${bullets[i]}" -lt 1 ]; then
  	    let "bullets[i] = 0"
        let "bullets[i-1] = 0"
      fi
	  fi
  done

	#incriment the gobbler bullets
	for ((i=1; i<${#gobblersbul[@]}; i+=2))
  do
	  if [ "${gobblersbul[i-1]}" -ne 0 ]; then
		  let "gobblersbul[i]++"		
			if [ "${gobblersbul[i]}" -gt $(($gameheight+1)) ]; then
  			let "gobblersbul[i] = 0"
        let "gobblersbul[i-1] = 0"
      fi
	  fi
  done

	#incriment the moneys
	for ((i=1; i<${#money[@]}; i+=2))
  do
	  if [ "${money[i-1]}" -ne 0 ]; then
		  let "money[i]++"		
			if [ "${money[i]}" -gt $(($gameheight+1)) ]; then
  			let "money[i] = 0"
        let "money[i-1] = 0"
      fi
	  fi
  done

  #echo "${bullets[*]}"
  #echo "${gobblersbul[*]}"
  #echo "${gobblers[*]}"
  #echo "${gobblersold[*]}"

  case $level in 
    1) levelone;;
    2) leveltwo;;
    3) levelthree;;
    4) levelfour;;
    *) win;;    
  esac
	
  countremaining    
  if [ "$remaining" -eq 0 ]; then
    let 'level++'
    resetbullets
    let 'levelloaded=0'    
  fi
  
    
	let "timer++"
	#sleep 0.005
done #end game loop

